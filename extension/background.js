let port = browser.runtime.connectNative("de.friz64.silver_cord");

port.onDisconnect.addListener((p) => {
    if (p.error) {
        console.log(`Disconnected from the native app: ${p.error.message}`);
    }
});

browser.runtime.onConnect.addListener((p) => {
    if (p.name === "background") {
        p.onMessage.addListener(port.postMessage);
    }
});
