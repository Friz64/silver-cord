const nodes = {
    playing: {
        selector: "div[class^=playbackButton]",
        value: (parent) => {
            const playButtonAction = parent.querySelector("button").getAttribute("data-test");
            if (playButtonAction === "pause") return true;
            if (playButtonAction === "play") return false;
            return null;
        },
    },
    progress: {
        selector: "time[data-test=current-time]",
        value: (node) => node.textContent,
    },
    duration: {
        selector: "time[data-test=duration]",
        value: (node) => node.textContent,
    },
    title: {
        selector: "div[class^=trackTitleContainer]",
        value: (node) => node.textContent,
    },
    trackId: {
        selector: "div[class^=trackTitleContainer]",
        value: (node) => Number(node.querySelector("a").getAttribute("href").split("/").pop()),
    },
    artists: {
        selector: "div[class^=currentMediaItemDetails]",
        value: (parent) => Array.from(parent.querySelector("span[class=artist-link]").childNodes)
            .map((node) => node.textContent),
    },
    albumCover: {
        selector: "figure[data-test=current-media-imagery]",
        value: (parent) => getHighestResImg(parent.querySelector("img")),
    },
    playingFrom: {
        selector: "div[class^=playingFrom]",
        value: (parent) => parent.lastElementChild.textContent,
    },
};

const port = browser.runtime.connect({ name: "background" });

let state = {};

for (const [name, query] of Object.entries(nodes)) {
    tryRepeatedly(() => {
        const element = document.querySelector(query.selector);
        if (element === null) { return false; }

        const value = query.value(element);
        if (value !== null && value !== undefined) {
            state[name] = value;
        }

        return true;
    }, 100, 100);
}

port.postMessage(state);

new MutationObserver((mutationList, observer) => {
    let dirty = false;
    for (const mutation of mutationList) {
        for (const [name, query] of Object.entries(nodes)) {
            const mutationTargetElement = mutation.target instanceof Element
                ? mutation.target : mutation.target.parentElement;
            const queryElement = mutationTargetElement.closest(query.selector);
            if (queryElement === null) { continue; }

            const value = query.value(queryElement);
            if (value !== null && value !== undefined && state[name] !== value) {
                dirty = true;
                state[name] = value;
            }
        }
    }

    if (dirty) { port.postMessage(state); }
}).observe(document.documentElement, {
    subtree: true,
    characterData: true,
    attributes: true,
    childList: true
});

function tryRepeatedly(functionRef, tries, interval) {
    setTimeout((remainingTries) => {
        if (functionRef() === false && remainingTries > 0) {
            tryRepeatedly(functionRef, remainingTries - 1, interval);
        }
    }, interval, tries);
}

// https://stackoverflow.com/a/60487971
function getHighestResImg(element) {
    if (element.getAttribute("srcset")) {
        return element
            .getAttribute("srcset")
            .split(",")
            .reduce(
                (acc, item) => {
                    let [url, width] = item.trim().split(" ");
                    width = parseInt(width);
                    if (width > acc.width) return { width, url };
                    return acc;
                },
                { width: 0, url: "" }
            ).url;
    }

    return element.getAttribute("src");
}
