#!/bin/bash
SCRIPT_LOCATION=$(readlink -f $(dirname $0))
JSON_PATH="$HOME/.mozilla/native-messaging-hosts/de.friz64.silver_cord.json"
echo "Installing native manifest file to '$JSON_PATH'"
mkdir -p $(dirname $JSON_PATH)
tee $JSON_PATH <<EOF
{
  "name": "de.friz64.silver_cord",
  "description": "Native host, used for sending data to discord",
  "path": "$SCRIPT_LOCATION/target/release/silver-cord",
  "type": "stdio",
  "allowed_extensions": ["silver-cord@friz64.de"]
}
EOF
