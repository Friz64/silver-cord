use anyhow::{bail, Context, Result};
use discord_sdk::{
    activity::{ActivityBuilder, Assets, Button},
    wheel::{UserState, Wheel},
    AppId, Discord, DiscordApp, Subscriptions,
};
use serde::Deserialize;
use std::time::{Duration, SystemTime};
use tokio::io::{self, AsyncReadExt, BufReader};

const APP_ID: AppId = 1182558156627378236;

#[derive(Deserialize, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
struct SignificantState {
    playing: Option<bool>,
    progress: Option<String>,
    duration: Option<String>,
    title: Option<String>,
    track_id: Option<u64>,
    artists: Option<Vec<String>>,
    album_cover: Option<String>,
    playing_from: Option<String>,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
#[serde(rename_all = "camelCase")]
struct State {
    progress: Option<String>,
    #[serde(flatten)]
    significant: SignificantState,
}

impl State {
    fn start_time(&self) -> Option<SystemTime> {
        let Some(progress) = &self.progress else {
            return None;
        };

        let mut parts = progress.split(':').rev();
        let mut duration = Duration::ZERO;
        duration += Duration::from_secs(parts.next().expect("seconds").parse().unwrap());
        duration += Duration::from_secs(parts.next().expect("minutes").parse().unwrap()) * 60;
        if let Some(hours) = parts.next() {
            duration += Duration::from_secs(hours.parse().unwrap()) * 60 * 60;
        }

        SystemTime::now().checked_sub(duration)
    }

    fn activity(&self, start_time: Option<SystemTime>) -> Option<ActivityBuilder> {
        fn smart_truncate(prefix: &str, text: &str, suffix: &str) -> String {
            let truncate_text = "...";
            let target_bytes: usize = 128;
            let actual_bytes = prefix.len() + text.len() + suffix.len();
            let surplus = actual_bytes as isize - target_bytes as isize;

            let mut text = String::from(text);
            let truncate = surplus > 0;
            if truncate {
                let mut new_len = (text.len() - (surplus as usize) - truncate_text.len())
                    .max(truncate_text.len());

                while !text.is_char_boundary(new_len) {
                    new_len -= 1;
                }

                text.truncate(new_len);
            }

            format!(
                "{prefix}{text}{}{suffix}",
                if truncate { truncate_text } else { "" }
            )
        }

        if self.significant.playing != Some(true) {
            return None;
        }

        let mut builder = ActivityBuilder::default();
        if let Some(title) = &self.significant.title {
            let mut suffix = String::from("\"");
            if let Some(duration) = &self.significant.duration {
                suffix += &format!(" - {duration}");
            }

            builder = builder.details(smart_truncate("\"", title, &suffix));
        }

        if let Some(artists) = &self.significant.artists {
            builder = builder.state(smart_truncate("by ", &artists.join(", "), ""));
        }

        if let Some(start_time) = start_time {
            builder = builder.start_timestamp(start_time);
        }

        let mut assets = Assets::default();

        if let Some(playing_from) = &self.significant.playing_from {
            assets = assets.small(
                "tidal_official_upscaled",
                Some(smart_truncate("playing from: ", playing_from, "")),
            );
        }

        if let Some(album_cover) = &self.significant.album_cover {
            assets = assets.large(album_cover, None::<String>);
        }

        if let Some(track_id) = &self.significant.track_id {
            builder = builder.button(Button {
                label: "stream from platform of choice".into(),
                url: format!("https://tidal.com/browse/track/{track_id}?u"),
            });
        }

        builder = builder.assets(assets).button(Button {
            label: "visit my last.fm".into(),
            url: include_str!("../last-fm-url").into(),
        });

        Some(builder)
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let (wheel, wheel_handler) = Wheel::new(Box::new(|error| eprintln!("discord error: {error}")));
    let mut user = wheel.user().0;
    let discord = Discord::new(
        DiscordApp::PlainId(APP_ID),
        Subscriptions::ACTIVITY,
        Box::new(wheel_handler),
    )?;

    eprintln!("connecting to discord");
    user.changed().await.unwrap();
    if !matches!(*user.borrow(), UserState::Connected(..)) {
        bail!("failed to connect to discord");
    }

    let mut state: State;
    let mut prev_significant = None;
    let mut prev_start_time = None;
    let mut stdin = BufReader::new(io::stdin());
    let mut length = 0u32;
    let mut update_bytes = Vec::new();
    eprintln!("connected, awaiting state updates");
    while let Ok(_bytes) = stdin.read_exact(bytemuck::bytes_of_mut(&mut length)).await {
        update_bytes.resize(length as usize, 0);
        stdin.read_exact(update_bytes.as_mut()).await?;
        // eprintln!("got (raw) '{}'", String::from_utf8_lossy(&update_bytes));
        state = serde_json::from_slice(&update_bytes).context("deserialization failed")?;
        let start_time = state.start_time();

        if match (prev_significant, (start_time, prev_start_time)) {
            (Some(prev_significant), _) if prev_significant != state.significant => true,
            (_, (Some(start_time), Some(prev_start_time))) => {
                let diff = start_time
                    .duration_since(prev_start_time)
                    .map_or_else(|err| err.duration(), |ok| ok);
                diff > Duration::from_secs(1)
            }
            _ => true,
        } {
            eprintln!("updating activity from {state:?}");
            if let Err(err) = match state.activity(start_time) {
                Some(activity) => discord.update_activity(activity).await,
                None => discord.clear_activity().await,
            } {
                eprintln!("activity update failed: {err}")
            }
        }

        prev_significant = Some(state.significant);
        prev_start_time = start_time;
    }

    discord.disconnect().await;
    Ok(())
}
